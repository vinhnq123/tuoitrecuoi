$(document).ready(function () {


    $('.slider-1').slick({
        infinite: true,
        slidesToShow: 2,
        slidesToScroll: 1,
        autoplay: false,
        dots: false,
        arrow: false
    });
    $('.slider-author').slick({
        infinite: true,
        slidesToShow: 9,
        slidesToScroll: 3,
        autoplay: false,
        dots: false,
        arrow: false
    });
    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        adaptiveHeight: true,
        asNavFor: '.slider-nav'
    });
    $('.slider-nav').slick({
        slidesToShow: 6,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        focusOnSelect: true

    });
    $('.slide-4thumbs').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        arrow: false
    });
    $('.slide-authors').slick({
        variableWidth: true,
        infinite: false
    });
    $('.slide-tags').slick({
        variableWidth: true,
        infinite: false
    });

    $('#commentModal').one('shown.bs.modal', function (e) {
        $(".slick-modal").slick({
            arrow: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
            fade: true
        });

    });
    $(".lazyload").lazyload({
        effect: "fadeIn",
        threshold: 200
    });

    $('.sidebar .block-bar:last-child, .block-left .box-left:last-child').addClass("sticky-top");


    $('.owl-carousel-1').owlCarousel({
        loop: true,
        margin: 20,
        nav: true,
        autoplay: true,
        autoplayTimeout: 5000,
        nav: true,
        smartSpeed: 600,
        responsive: {
            1000: {
                items: 6
            }
        }
    });

    $('.owl-carousel-2').owlCarousel({
        loop: false,
        margin: 20,
        nav: true,
        autoplayTimeout: 5000,
        nav: true,
        smartSpeed: 600,
        responsive: {

            1000: {
                items: 4
            }
        }
    });
    $('.slider-comic').owlCarousel({
        loop: true,
        margin: 0,
        nav: true,

        autoplayTimeout: 5000,
        nav: true,
        smartSpeed: 600,
        responsive: {

            1000: {
                items: 2
            }
        }
    });
    $('.owl-carousel-3').owlCarousel({
        loop: false,
        margin: 20,
        nav: true,
        autoplayTimeout: 5000,
        nav: true,
        smartSpeed: 600,
        responsive: {
            1000: {
                items: 4
            }
        }
    });
    $('.owl-carousel-4').owlCarousel({
        loop: true,
        center: true,
        margin: 10,
        nav: true,
        autoplayTimeout: 5000,
        nav: true,
        smartSpeed: 600,
        responsive: {
            1000: {
                items: 4
            }
        }
    });
    $('.slider-viewmore').owlCarousel({
        loop: true,
        margin: 20,
        nav: true,
        autoplayTimeout: 5000,
        nav: true,
        smartSpeed: 600,
        items: 4
    });
    
    $('.slider-video-1').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        focusOnSelect: true
    });
    
    if ($(".scroll-custom").length > 0) {
        $(".scroll-custom").mCustomScrollbar();
    }
    $(window).scroll(function () {
        if ($(this).scrollTop() > 200) {
            $('.btn-more-than').addClass('show');
            $('.sticky-header').addClass('show');
        } else {
            $('.btn-more-than').removeClass('show');
            $('.sticky-header').removeClass('show');
        }
    });
    if ($(".btn-more-than").length > 0) {
        $('.btn-more-than').on('click', function () {
            $("html, body").animate({ scrollTop: 0 }, 1000, 'easeInOutExpo');
            return false;
        });
    }

});