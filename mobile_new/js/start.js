$(()=>{

    $('.slider-1').slick({
        infinite: true,
        slidesToShow: 2,
        slidesToScroll: 1,
        autoplay: false,
        dots: false,
        arrow: false
    });
    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        adaptiveHeight: true,
        asNavFor: '.slider-nav'
    });
    $('.slider-nav').slick({
        slidesToShow: 6,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        focusOnSelect: true

    });
    $('.slide-4thumbs').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        arrow: false
    });
    $('.slide-authors').slick({
        slidesToShow: 6,
        
        infinite: false
    });
    $('.slide-tags').slick({
        variableWidth: true,
        infinite: false
    });
    $('.modal#commentModal').on('shown.bs.modal', function (e) {
        $(".slide-comment").slick({
            arrow:true,
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite:false,
            prevArrow: $('.comic-comment-prev'),
            nextArrow: $('.comic-comment-next')
        });
    });


    $('.slider-b').slick({
        infinite: true,
        slidesToShow: 1.5,
        slidesToScroll: 1,
        autoplay: false,
        dots: false,
        arrow: false
    });

    //multi modal show up
    $(document).on('show.bs.modal', '.modal', function () {
        $("body").addClass("modal-open");
        let zIndex = 1040 + (10 * $('.modal:visible').length);
        $(this).css('z-index', zIndex);
        setTimeout(function () {
            $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
        }, 0);
        if (!$(this).is($("#stickerModal"))){
            $('.modal').not($(this)).modal('hide');
        }
    });

    // multi modal hide
    $('.modal').on("hidden.bs.modal", function (e) {
        if ($('.modal:visible').length) {
            $('.modal-backdrop').first().css('z-index', parseInt($('.modal:visible').last().css('z-index')) - 10);
            $('body').addClass('modal-open');
        }
    });
});
