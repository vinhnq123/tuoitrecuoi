$(document).ready(function () {
    new CountComment();
});

/*class  count comment*/
class CountComment {
    constructor() {
        CountComment.getCountComment();
    }

    static getCountComment() {
        $('.bar-comment[data-linkgetcount]').each(function (v) {
            let e = $(this);
            let url = AppConfig.domainCdnHtml + '/' + e.attr('data-linkgetcount');
            $.getJSON(url, function (numberComment) {
                if (numberComment > 0) {
                    e.append(`<span class="outer-icon"><span class="number">${numberComment}</span><span class="icon">&#61704</span></span>`);
                } else {
                    e.removeClass('hasComment');
                }
            });
            e.removeAttr('data-linkgetcount');
        });
    }
}

/*helper for comment.js */
const helper = {
        ARRAY_GLOBAL_COLOR: ['#172b4d', '#5e72e4', '#11cdef', '#2dce89', '#f5365c', '#fb6340', '#bd081c', '#3b5999'],
        //random color for avatar images
        randomColorInArray: (array) => {
            return array[Math.floor(Math.random() * array.length)]
        },
        isVisibleInViewport: (elem) => {
            let y = elem.offsetTop;
            let height = elem.offsetHeight;

            while (elem === elem.offsetParent)
                y += elem.offsetTop;

            let maxHeight = y + height;
            return (y < (window.pageYOffset + window.innerHeight)) && (maxHeight >= window.pageYOffset);

        },
        timeSince: (date) => {

            let seconds = Math.floor((new Date() - date) / 1000);

            let interval = Math.floor(seconds / 31536000);

            if (interval >= 1) {
                return interval + " năm trước";
            }
            interval = Math.floor(seconds / 2592000);
            if (interval >= 1) {
                return interval + " tháng trước";
            }
            interval = Math.floor(seconds / 86400);
            if (interval >= 1) {
                return interval + " ngày trước";
            }
            interval = Math.floor(seconds / 3600);
            if (interval >= 1) {
                return interval + " giờ trước";
            }
            interval = Math.floor(seconds / 60);
            if (interval >= 1) {
                return interval + " phút trước";
            }
            return Math.floor(seconds) + " giây trước";
        },
        //check regex email (email = <input/>)
        isEmail:
            (email) => {
                let regex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/igm;
                return regex.test(email.val());
            },
        //check validate email (email = <input/>)
        checkEmail:
            email => {
                if (email.val() === "") {
                    email.next(".error").text("Hãy nhập Email !").show();
                    return false
                } else {
                    if (helper.isEmail(email)) {
                        email.next(".error").hide();
                        return true
                    } else {
                        email.next(".error").text("Email bạn nhập không đúng !").show();
                        return false
                    }
                }
            },
        //slide content to show
        renderContent:
            (content, posCut = 10) => {
                if (content === "") {
                    return "";
                }
                let lengthContent = content.length;
                if (lengthContent >= posCut * 2) {
                    let contentShow = content.slice(0, posCut);
                    return `<pre class="ctn-short">${contentShow}...<a> &nbsp;<i class="fa fa-plus-square-o" aria-hidden="true"></i></a></pre>
            <pre class="ctn-full" style="display: none">${content}<a>&nbsp;<i class="fa fa-minus-square-o" aria-hidden="true"></i></a></pre>`;
                }
                return `<pre>${content}</pre>`;
            },
        //check validate content (content = <input/>)
        checkContent:
            (content, ticker = false) => {
                if (ticker) {
                    content.next(".error").hide();
                    return true
                } else {
                    let contentCookie = helper.getCookie('cm-ttc');
                    if (content.val() === "") {
                        content.next(".error").text("Vui lòng nhập bình luận !").show();
                        return false;
                    }
                    if (contentCookie === content.val()) {
                        content.next(".error").text("Bạn vừa nhập bình luận này rồi !").show();
                        return false;
                    }
                    let name = content.parents(".comment-box").siblings(".comment-content").find(".comment-name").text();
                    let replyFor = `@${name.trim()}`;
                    let realContent = content.val().replace(replyFor, "");
                    if (realContent.trim() === "") {
                        content.next(".error").text("Vui lòng nhập bình luận !").show();
                        return false
                    } else {
                        content.next(".error").hide();
                        return true
                    }
                }
            },
        // set cookie
        setCookie:
            (name, comment, duration) => {
                let oldData = helper.getCookie(name);
                if (oldData) {
                    oldData = JSON.parse(oldData);
                } else oldData = [];
                oldData.push(comment);
                let date = new Date();
                date.setTime(date.getTime() + (duration * 60 * 1000));
                let expires = "expires=" + date.toUTCString();
                document.cookie = name + "=" + JSON.stringify(oldData) + ";" + expires;
            },
        //set cookie comment
        setCookieComment:
            (name, content, duration) => {
                let date = new Date();
                date.setTime(date.getTime() + (duration * 60 * 1000));
                let expires = "expires=" + date.toUTCString();
                document.cookie = name + "=" + content + ";" + expires;
            },
        // get cookie
        getCookie:
            (name) => {
                let value = document.cookie.match('(^|;) ?' + name + '=([^;]*)(;|$)');
                return value ? value[2] : null;
            },
        // remove emoji in comment's content
        removeEmoji:
            (string) => {
                let regex = /(?:[\u2700-\u27bf]|(?:\ud83c[\udde6-\uddff]){2}|[\ud800-\udbff][\udc00-\udfff]|[\u0023-\u0039]\ufe0f?\u20e3|\u3299|\u3297|\u303d|\u3030|\u24c2|\ud83c[\udd70-\udd71]|\ud83c[\udd7e-\udd7f]|\ud83c\udd8e|\ud83c[\udd91-\udd9a]|\ud83c[\udde6-\uddff]|\ud83c[\ude01-\ude02]|\ud83c\ude1a|\ud83c\ude2f|\ud83c[\ude32-\ude3a]|\ud83c[\ude50-\ude51]|\u203c|\u2049|[\u25aa-\u25ab]|\u25b6|\u25c0|[\u25fb-\u25fe]|\u00a9|\u00ae|\u2122|\u2139|\ud83c\udc04|[\u2600-\u26FF]|\u2b05|\u2b06|\u2b07|\u2b1b|\u2b1c|\u2b50|\u2b55|\u231a|\u231b|\u2328|\u23cf|[\u23e9-\u23f3]|[\u23f8-\u23fa]|\ud83c\udccf|\u2934|\u2935|[\u2190-\u21ff])/g;
                return string.replace(regex, '');
            },
        //make comment holder
        createCommentHolder() {
            return `<div class="item-comment item-comment__holder">
    <div class="comment media">
        <a href="javascript:void(0)" class="img-user ani-bg"></a>
        <div class="media-body">
            <div class="inner-cm">
                <h5 class="mt-0 ani-bg"></h5>
                <pre class="ani-bg"> </pre>
                <pre class="ani-bg"> </pre>
                <div class="tool-like">
                    <span class="ani-bg"></span>
                    <span class="ani-bg"></span>
                    <span class="ani-bg"></span>
                </div>
            </div>
        </div>
    </div>
</div>`
        }

    }
;