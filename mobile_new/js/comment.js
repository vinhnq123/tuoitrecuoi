// require import lib-comment.js before\

/*
@author Duynn
@time 16-10-2019
@email wasahara11111@gmail.com
*/

/*
class comment
* 1. constructor
* 2. init
* 3. generate block
* 4. event (load , write , like , get sticker , report , send ...)
* 5. map data to DOM
* 6. load more comment
* 7. write comment
* 8. create sticker
* 9. like comment
* 10. send comment
* 11. report comment
* */
class Comment {

    /* 1. cons*/
    constructor(keySortLike, keySortDate = "") {
        this.urlCommentLike = keySortLike;
        this.urlCommentDate = keySortDate;
        this.urlSticker = AppConfig.domainCdnHtml + "/" + AppConfig.keyThemeSticker + ".json";
        this.stickerList = [];
        this.stickerListTheme = [];
        this.commentDate = {};
        this.commentLike = {};
        this.appId = 15;
        this.cookieLikeComment = JSON.parse(helper.getCookie("like-ttc") ? helper.getCookie("like-ttc") : "[]");
        this.init();
    }

    /* 2. contains initial comment*/
    init() {
        this.event();
        let container = $(".plugin-comment");
        container.prepend(helper.createCommentHolder());
        let ajaxSticker = $.ajax(
            {
                url: this.urlSticker,
                timeout: 5000
            }
        );
        ajaxSticker
            .fail(err => {
                let emoji = this.createSticker(this.stickerList);
                $(".block-comment").prepend($(this.writeComment(emoji)));
                this.initComment(this.urlCommentLike, "like")
            })
            .done(stickers => {
                this.stickerList = stickers ? JSON.parse(stickers).data : [];
                let emoji = this.createSticker(this.stickerList);
                $(".block-comment").prepend($(this.writeComment(emoji)));
                this.initComment(this.urlCommentLike, "like")
            });
    }

    /* 3. initial comment*/
    initComment(urlComment, type) {
        let ajaxComments = $.ajax(
            {
                url: AppConfig.domainCdnHtml + "/" + urlComment + "1.json",
            }
        );
        ajaxComments
            .done((listComments) => {
                if (Object.keys(JSON.parse(listComments)).length > 0) {
                    if (type === "like") {
                        this.commentLike = JSON.parse(listComments);
                        this.commentLike.pageNum = 1;
                        this.commentLike.currentNum = this.commentLike.number[0];
                        this.generateBlockComment(this.commentLike, type);
                    } else if (type === "date") {
                        this.commentDate = JSON.parse(listComments);
                        this.commentDate.pageNum = 1;
                        this.commentDate.currentNum = this.commentLike.number[0];
                        this.generateBlockComment(this.commentDate, type);
                    }
                }
            }).fail(err => {
            $(".plugin-comment").empty();
        })
    }

    generateBlockComment(resolve, type) {
        let emoji = this.createSticker(this.stickerList);
        let comments = resolve.data;
        //prepend tool comment
        let container = $(".plugin-comment");
        container.empty();
        if (comments.length > 0) {
            let toolComment = `<div class="tool-comment">
                                        <h4>BĂ¬nh luáº­n (${resolve.total})</h4>
                                        <span class="fr">                                          
                                            <a class="${type === "like" ? "active" : ""}" href="javascript:;" data-filter="like">Ná»•i báº­t</a>
                                            <a class="${type === "date" ? "active" : ""}" href="javascript:;" data-filter="date">Má»›i nháº¥t</a>
                                        </span>
                                    </div>`;
            if (this.urlCommentDate !== "") {
                //add to DOM
                container.prepend(toolComment);
            }
            let wrap = $('<div class="wrapper-comment"></div>');
            container.append(wrap);
            //map data to comment
            comments.forEach((comment, index) => {

                wrap.append(`${this.mapDataToComment(comment, index, emoji)}`)
            });
            if (resolve.currentNum < resolve.total_parent && resolve.number.length > 1) {
                let btnShowMore = `<a href="" class="viewmore" data-filter="${type}" style="">Xem thĂªm  (${resolve.number[resolve.pageNum]}) bĂ¬nh luáº­n</a>`;
                wrap.append(btnShowMore);
            }
            container.append(`<a href="#box-reply-cmt" class="btn-writecomment" style="">Viáº¿t bĂ¬nh luáº­n...</a>`);

        }
    }

    /* 4. contains all handle event*/
    event() {

        //handle load more comment
        this.eventLoadMoreComment();

        // handle write comment
        this.eventWriteComment();

        //show more comment
        this.eventShowMoreComment();

        // like comment
        this.eventLikeComment();

        //get sticker
        this.eventGetSticker();

        //handle report
        this.eventReportComment();

        //handle send comment
        this.eventSendComment();

        //handle switch list
        this.eventSwitchComment();

        //others
        this.eventOthers();
    }

    eventLoadMoreComment() {
        $(document).on("click", ".viewmore", e => {
            $(e.currentTarget).before(helper.createCommentHolder());
            let type = $(e.currentTarget).data('filter');
            $(e.currentTarget).hide();
            e.preventDefault();
            let emoji = this.createSticker(this.stickerList);
            this.loadMore(type, (data) => {
                let moreCmt = data.data.slice(data.currentNum - data.number[data.pageNum - 1]);
                $(".item-comment__holder").remove();
                if (moreCmt.length > 0) {
                    moreCmt.forEach((comment, index) => {
                        $(e.target).before(`${this.mapDataToComment(comment, index, emoji)}`);
                    });
                }
                if (data.currentNum >= data.total_parent)
                    $(e.target).remove();
                else {
                    $(e.target).show();
                    $(e.target).html(`Xem thĂªm  (${data.number[data.pageNum]}) bĂ¬nh luáº­n`);
                }
            });
        });
    }

    eventWriteComment() {
        //write comment
        $(document).on('click', '.btn-writecomment', e => {
            e.preventDefault();
            let anchor = $(e.currentTarget);
            $('html, body').animate({
                scrollTop: ($(anchor.attr('href')).offset().top - 200)
            }, 1000);
            $(anchor.attr('href')).focus();
        });
        // show box for enter info reply comment
        $(document).on("click", '.media-body .reply', function (e) {
            let text = $(this).siblings(".box-reply-cm").find("textarea").val();
            let boxCm = $(this).siblings(".box-reply-cm");
            boxCm.slideToggle().find("textarea").val("").val(text).focus();
            $(".wrapper-comment").find(".tool-like>.box-reply-cm")
                .not(boxCm)
                .slideUp();
        });
        // toggle box-info
        $(document).on('focus', ".box-reply-cm", function () {
            $(this).parents(".comment-write").find(".box-info").slideDown();
            $(this).parents(".box-reply-cm").find(".box-info").slideDown();
        });
        // event focus input name
        $(document).on('focus', 'input[name=inputName]', function () {
            let email = $(this).parent().prev().find('input[name=inputEmail]').val();
            let name = email.replace(/@.*/, "");
            $(this).val(name);
        });
        $(document).on('focus', '.block-comment textarea,.block-comment input,.modal input', function () {
            $(this).next(".error").fadeOut();
        });

    }

    eventLikeComment() {
        //like comment
        $(document).on("click", '.tool-like .like', e => {
            let objectId = $(e.target).parents('article').attr('data-oid')
                ? $(e.target).parents('article').attr('data-oid') : $("input[name=hidObjectId]").val(),
                commentId = $(e.target).parents(".tool-like").data("cmid");
            let likeField = $(e.currentTarget).find('.number-like');
            if (this.cookieLikeComment.find(comment => comment["comment-id"] === commentId) === undefined) {
                this.likeComment(objectId, commentId, likeField);
                $(e.currentTarget).addClass('active');
                $(e.currentTarget).find('i.fa').removeClass('fa-heart-o').addClass('fa-heart');
            }
        });
        $(document).on("click", '.tool-like .like .fa', function () {
            $(this).addClass('heart-beat');
            $(this).one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function (event) {
                $(this).removeClass('heart-beat');
            });
        });

    }

    eventGetSticker() {
        //handle chose sticker
        $(document).on('click', '.btn-group-sticker ul.list-sticker li', function () {
            let sources = $(this).attr('data-src'), icon = $(this).attr('data-stick');
            $(this).parents('div.dropdown-menu.dropdown-menu-right').removeClass('show');
            $(this).parents('div.btn-group-sticker').removeClass('show');
            let choStick = $(this).parents('div.btn-group-sticker').parent().siblings('.chosen-ticker');
            $(this).parents(".box-reply-cm").find(".box-info").slideDown();
            $(this).parents(".comment-write").find(".box-info").slideDown();
            choStick.attr('data-src', sources);
            choStick.attr('data-sticker', icon);
            let img = document.createElement('img');
            img.src = sources;
            img.setAttribute('width', '100px');
            img.setAttribute('height', '100px');
            choStick.html(img);
        }); // event choose sticker for comment's sticker

        $(document).on('click', '.slick-sticker .i-sticker', e => {
            const themeId = $(e.currentTarget).data('themeid');
            this.findStickerInLocal(themeId, e);

        }); // get list sticker for sticker's theme

        /* handle event*/
        $(document).on("click", ".btn-sticker", e => {
            let id = parseInt($(e.target).parents(".btn-group-sticker").find(".i-sticker[data-index=1]").data("themeid"));
            this.findStickerInLocal(id, e);
        });

    }

    eventReportComment() {
        $(document).on('click', '.tool-like .report', function () {
            let cmid = $(this).parent().attr('data-cmid'),
                oid = $(this).parents('article').attr('data-oid')
                    ? $(this).parents('article').attr('data-oid')
                    : $("input[name=hidObjectId]").val();
            $('input[name=hidCCommentId]').val(cmid);
            $('input[name=hidCObjectId]').val(oid);
        }); // get info comment before report's popup is show

        $("select#reason").change(function () {
            let selected = $(this).children("option:selected").val();
            if (parseInt(selected) === 0) {
                $("#warningModal .box-other-reason").show();
            } else {
                $("#warningModal .box-other-reason").hide();
                $('textarea').val('');
            }
        }); // get reason for comment's report

        $(document).on('click', '#warningModal .btn-send-1', e => {
            let cmId = $('input[name=hidCCommentId]').val(),
                reInput = $('input[name=reporter_email]'),
                re = reInput.val(),
                oReason = $('#warningModal textarea').val(),
                reason = $('select#reason').find("option:selected").val(),
                oId = $('input[name=hidCObjectId]').val();
            if (helper.checkEmail(reInput))
                this.sendReport(cmId, oId, re, oReason, reason);
        }); // report comment
    }

    eventShowMoreComment() {
        $(document).on('click', '.btn-moreSubComment', function () {
            $(this).nextAll(".media").slideDown();
            $(this).remove();
        });
    }

    eventSendComment() {
        $(document).on("click", '.block-comment .btn-comment', e => {
            const data = this.getAllToSend(e);
            if (data) {
                this.sendComment(data);
                $(e.currentTarget).parents(".box-reply-cm").find("textarea").val("");
                $(e.currentTarget).parents(".comment-write").find("textarea").val("");
                $(e.currentTarget).parents('.box-info').hide();
                $(e.currentTarget).parents(".box-info").siblings(".wrapper-txtarea").find(".chosen-ticker").attr('data-sticker',null).html('');
            }

        }); // comment for article
    }

    eventSwitchComment() {
        $(document).on("click", ".tool-comment a", e => {
            $(e.currentTarget).addClass("active").siblings("a").removeClass("active");
            let type = $(e.currentTarget).data("filter");
            if (type === "like") {
                if (Object.keys(this.commentLike).length === 0)
                    this.initComment(this.urlCommentLike, type);
                else
                    this.generateBlockComment(this.commentLike, type)
            } else if (type === "date") {
                if (Object.keys(this.commentDate).length === 0)
                    this.initComment(this.urlCommentDate, type);
                else
                    this.generateBlockComment(this.commentDate, type)
            }
        })
    }

    eventOthers() {
        $(document).on('click', '.btn-group-sticker .btn-prev', function () {
            let nextButton = $(this).siblings(".btn-next");
            nextButton.show();
            let themeSticker = $(this).next().find('div.i-sticker');
            let length = themeSticker.length;
            let countClick = parseInt($(this).attr('data-click'));
            nextButton.attr('data-click', countClick - 1);
            let vtShow = countClick - 1;
            $(this).attr('data-click', vtShow);
            $(this).next().find(`div.i-sticker[data-index=${vtShow}]`).show();
            if (vtShow === 1) {
                $(this).hide();
            }
        });

        $(document).on('click', '.btn-group-sticker .btn-next', function () {
            let prevButton = $(this).siblings(".btn-prev");
            prevButton.show();
            let themeSticker = $(this).prev().find('div.i-sticker');
            let length = themeSticker.length;
            let countClick = parseInt($(this).attr('data-click'));
            $(this).prop('data-click', countClick + 1);
            prevButton.attr('data-click', countClick + 1);
            let last = 5 + countClick;
            $(this).prev().find(`div.i-sticker[data-index=${last - 5}]`).hide();
            if (last >= length) {
                $(this).hide();
            }
        });
        $(document).on("click", "pre a .fa", function () {
            let boxContent = $(this).parents("pre");
            if ($(this).hasClass('fa-plus-square-o')) {
                boxContent.hide();
                boxContent.siblings().show();
                return '';
            }
            boxContent.hide();
            boxContent.siblings('pre').show();

        });
    }


    /* 5. map data to parent comment*/
    mapDataToComment(comment, index = 0, emoji) {
        let userImg = comment.author_name.slice(0, 1).toUpperCase();
        let sticker = comment.sticker ? (comment.sticker.length !== 0 ? `<div class="chosen-ticker"><img src="${comment.sticker.avatar_base_url + "/" + comment.sticker.avatar_path}" alt=""/></div>` : "") : "";
        let childComment = comment.child_comments ? (comment.child_comments !== '' ? this.mapDataToChildComment(comment.child_comments, emoji) : "") : "";
        return `<div class="item-comment" data-id="${comment.id}" style="animation-delay: ${index / 20}s">
                <div class="comment media">
                        <a class="img-user" style="background-color: ${helper.randomColorInArray(helper.ARRAY_GLOBAL_COLOR)}">${userImg}</a>
                    <div class="media-body">
                        <div class="inner-cm">
                            <h5 class="mt-0">${comment.author_name}</h5>
                            ${helper.renderContent(comment.content, 200)}
                            ${sticker}
                            <div class="tool-like active" data-cmid="${comment.id}" data-author="${comment.author_name}">
                                   ${this.findCommentLiked(parseInt(comment.id)) ? `<span class="like active"><i class="fa fa-heart" aria-hidden="true"></i> <span class="number-like">${comment.like_number}</span></span>
` : `                                <span class="like"><i class="fa fa-heart-o" aria-hidden="true"></i> <span class="number-like">${comment.like_number}</span></span>
`}
                                <span class="reply">Tráº£ lá»i</span>
                                <span class="report" title="Äá»c giáº£ bĂ¡o ná»™i dung vi pháº¡m" data-toggle="modal" data-target="#warningModal"> <i class="fa fa-flag" aria-hidden="true"></i></span>
                                <span class="date">${helper.timeSince(comment.created_at * 1000)}</span>
                                <div class="box-reply-cm" style="display:none;">
                                    <div class="wrapper-txtarea">
                                        <div class="chosen-ticker"></div>
                                        <div class="outer-txtarea mt-2">
                                            <textarea class="box-reply-cm" placeholder="BĂ¬nh tÄ©nh, tá»± tin, khĂ´ng cay cĂº">@${comment.author_name}&nbsp;</textarea>
                                            <i class="error error-content" style="display: none">Vui lĂ²ng nháº­p bĂ¬nh luáº­n !</i>
                                            ${emoji}
                                        </div>
                                    </div>
                                    <div class="box-info">
                                        <div class="row">
                                            <div class="col-sm-5">
                                                <input class="txt-erorr" type="text" name="inputEmail" placeholder="Email">
                                                <i class="error error-email" style="display: none">HĂ£y nháº­p Email !</i>
                                            </div>
                                            <div class="col-sm-5">
                                                <input type="text" name="inputName" placeholder="Há» vĂ  tĂªn">
                                                <i class="error error-name" style="display: none">HĂ£y nháº­p há» vĂ  tĂªn !</i>
                                            </div>
                                            <div class="col-sm-2 text-right">
                                                <button class="btn-comment">Gá»­i</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                ${comment.child_comments.length ?
            `<a class="btn-moreSubComment" href="javascript: void(0)">
                <span class="outer-rotate">
                    <i class="fa fa-reply" aria-hidden="true"></i>
                 </span> ${comment.child_comments.length} pháº£n há»“i</a>` +
            childComment
            :
            ""
        }                
            </div>`;
    }

    /* 5. map data to child comment*/
    mapDataToChildComment(comments, emoji) {
        let listChild = "";
        comments.forEach(comment => {
            let userImg = comment.author_name.slice(0, 1).toUpperCase();
            let sticker = comment.sticker ? (comment.sticker.length !== 0 ? `<div class="chosen-ticker"><img src="${comment.sticker.avatar_base_url + "/" + comment.sticker.avatar_path}" alt=""/></div>` : "") : "";
            listChild += `<div class="media mt-3" style="display: none" data-childId="${comment.id}" data-parentId="${comment.parent_id}">
<a class="img-user" style="background-color: ${helper.randomColorInArray(helper.ARRAY_GLOBAL_COLOR)}">${userImg}</a>
            <div class="media-body">
                <h5 class="mt-0">${comment.author_name}</h5>
                ${helper.renderContent(comment.content, 200)}
                ${sticker}
                <div class="tool-like active"  data-cmid="${comment.id}" data-author="${comment.author_name}">
                                                   ${this.findCommentLiked(parseInt(comment.id)) ? `<span class="like active"><i class="fa fa-heart" aria-hidden="true"></i> <span class="number-like">${comment.like_number}</span></span>
` : `                                <span class="like"><i class="fa fa-heart-o" aria-hidden="true"></i> <span class="number-like">${comment.like_number}</span></span>
`}
                    <span class="reply">Tráº£ lá»i</span>
                    <span class="report" title="Äá»c giáº£ bĂ¡o ná»™i dung vi pháº¡m" data-toggle="modal" data-target="#warningModal"> <i class="fa fa-flag" aria-hidden="true"></i></span>
                    <span class="date">${helper.timeSince(comment.created_at * 1000)}</span>
                    <div class="box-reply-cm" style="display:none;">
                        <div class="wrapper-txtarea">
                            <div class="chosen-ticker"></div>
                            <div class="outer-txtarea">
                                <textarea class="box-reply-cm" placeholder="BĂ¬nh tÄ©nh, tá»± tin, khĂ´ng cay cĂº">@${comment.author_name}&nbsp; </textarea>
                                <i class="error error-content" style="display: none">Vui lĂ²ng nháº­p bĂ¬nh luáº­n !</i>
                                ${emoji}
                            </div>
                        </div>
                        <div class="box-info">
                            <div class="row">
                                <div class="col-sm-5">
                                    <input class="txt-erorr" type="text" class="commentEmail" placeholder="Email">
                                    <i class="error error-email" style="display: none">Hãy nhập Email !</i>
                                </div>
                                <div class="col-sm-5">
                                    <input type="text" name="inputName" placeholder="Họ và tên">
                                    <i class="error error-name" style="display: none">Hãy nhập họ và tên !</i>
                                </div>
                                <div class="col-sm-2 text-right">
                                    <button class="btn-comment">Gá»­i</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
             </div>
        </div>`;
        });
        return listChild;
    }

    /* 6. show more comment from event load-more-comment*/
    loadMore(type, callback) {
        if (type === "like") {
            $.ajax({
                url: AppConfig.domainCdnHtml + "/" + this.urlCommentLike + `${this.commentLike.pageNum + 1}.json`,
            }).done(listComments => {
                let comments = JSON.parse(listComments);
                this.commentLike.currentNum += comments.number[this.commentLike.pageNum];
                this.commentLike.pageNum += 1;
                this.commentLike.data = this.commentLike.data.concat(comments.data);
                callback(this.commentLike);
            }).fail(err => {
                $(".block-comment a.viewmore").remove();
            })
            ;
        } else if (type === "date") {
            $.ajax({
                url: AppConfig.domainCdnHtml + "/" + this.urlCommentDate + `${this.commentDate.pageNum + 1}.json`,
            }).done(listComments => {
                let comments = JSON.parse(listComments);
                this.commentDate.currentNum += comments.number[this.commentDate.pageNum];
                this.commentDate.pageNum += 1;
                this.commentDate.data = this.commentDate.data.concat(comments.data);
                callback(this.commentDate);
            }).fail(err => {
                $(".block-comment a.viewmore").remove();
            })
            ;
        }

    }

    /* 7. write comment from event write-comment*/
    writeComment(emoji) {
        return ` <div class="comment-write">
            <h4>Viáº¿t bĂ¬nh luáº­n</h4>
            <div class="wrapper-txtarea">
                <div class="chosen-ticker"></div>
                <div class="outer-txtarea">
                    <textarea class="box-reply-cm" id="box-reply-cmt" placeholder="Comment Ä‘Ăª! Ai cáº¥m báº¡n cÆ°á»i..."></textarea>
                    <i class="error" style="display: none">Vui lĂ²ng nháº­p bĂ¬nh luáº­n !</i>
                    ${emoji}     
                </div>
            </div>
            <div class="box-info" style="display: none;">
                <div class="row">
                    <div class="col-sm-5">
                        <input type="text" name="inputEmail" placeholder="Email">
                        <i class="error error-email" style="display: none">HĂ£y nháº­p Email !</i>
                    </div>
                    <div class="col-sm-5">
                        <input type="text" name="inputName" placeholder="Há» vĂ  tĂªn">
                        <i class="error error-name" style="display: none">HĂ£y nháº­p há» vĂ  tĂªn !</i>
                    </div>
                    <div class="col-sm-2 text-right">
                        <button class="btn-comment">Gá»­i</button>
                    </div>
                </div>
            </div>
        </div>`;
    }

    /* 8. create sticker box*/
    createSticker() {
        let sticker = this.stickerList;
        let htmlSticker;
        if (sticker.length > 0) {
            htmlSticker = `<div class="btn-group-sticker">
                                        <div class="dropdown-menu">
                                            <div class="custom-sticker">
                                                <div class="top-sticker">
                                                    <div class="inner-sticker">
                                                        <div class="btn-prev" title="previous" data-click="1" style="display: none">
                                                            <i class="fa fa-angle-left" aria-hidden="true"></i>
                                                        </div>
                                                        <div class="slick-sticker">
                                                            <div class="inner-slider-sticker">`;
            sticker.forEach((sticker, index) => {
                const stickersUrl = sticker.avatar_base_url + sticker.avatar_path;
                htmlSticker += `<div class="i-sticker" data-themeId="${sticker.id}" data-index=${index + 1}>
                                    <span class="btn-popover" data-toggle="tooltip" data-placement="top" title="${sticker.description}">
                                        <img alt="" src="${stickersUrl}">
                                    </span>
                                </div>`;
            });
            htmlSticker += `</div></div>
                                    <div class="btn-next" title="next" data-click="1">
                                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="middle-sticker" >
                                    <ul class="list-sticker">
                                    </ul>
                            </div>
                        </div>
                    </div>
                </div>`;
        } else {
            htmlSticker = "";
        }
        return htmlSticker;
    }

    /* 8. render sticker from event get sticker*/
    renderStickerByTheme(themeId, callback) {
        let url = AppConfig.domainCdnHtml + "/" + AppConfig.keySticker;

        $.ajax({
            url: url + "_" + themeId + ".json",
        }).done(data => {
            let stickers = JSON.parse(data).data;
            let html = '';
            stickers.forEach(function (item, index) {
                html += `<li data-stick="${item.id}" data-src="${item.avatar_base_url + "/" + item.avatar_path}">
                            <span><img class="action" src="${item.avatar_base_url + "/" + item.avatar_path}"></span>
                        </li>`;
            });
            callback(html);
        }).fail(err => {
            let html = "";
            callback(html);
        })
        ;

    }

    /* 8. find sticker in local*/
    findStickerInLocal(id, e) {
        if (!this.checkSticker(id) || this.stickerListTheme.length === 0) {
            this.renderStickerByTheme(id, html => {
                this.stickerListTheme.push({
                    html,
                    id
                });
                $(e.target).parents(".btn-group-sticker")
                    .find('.list-sticker')
                    .html(html);
            });

        } else {
            let resolve = this.stickerListTheme.find(item => {
                return parseInt(item.id) === parseInt(id);
            });
            $(e.target).parents(".btn-group-sticker")
                .find('.list-sticker')
                .html(resolve.html);
        }
    }

    /* 8. true or false sticker in local*/
    checkSticker(id) {
        return this.stickerListTheme.find(sticker => parseInt(sticker.id) === id);
    }

    /* 9. like comment from event like-comment*/
    likeComment(objectId, commentId, likeField) {
        let likeNumber = likeField.text();
        let comment = {
            "article-id": objectId,
            "comment-id": commentId
        };
        $.ajax({
            url: AppConfig.idDomainCM + '/api/vote-comment',
            type: 'POST',
            data: {
                'c': commentId,
                'app_id': this.appId,
                'object_id': objectId
            },
            dataType: 'json',
            success: data => {
                if (data.success === true) {
                    likeField.text(parseInt(likeNumber) + 1);
                    helper.setCookie("like-ttc", comment, 1440); // 1 ngĂ y
                    this.cookieLikeComment = JSON.parse(helper.getCookie("like-ttc") ? helper.getCookie("like-ttc") : "[]");
                }
            }
        });
    }

    /* 9. true or false liked comment in cookie*/
    findCommentLiked(id) {
        return (this.cookieLikeComment.find(comment => comment["comment-id"] === id));
    }

    /* 9. get all value need to send comment*/
    getAllToSend(e) {
        // get data to send
        let objectId = $(e.currentTarget).parents("article").data("oid") || $("input[name=hidObjectId]").val() || "";
        let objectTitle = $(e.currentTarget).parents("article").data("title") || $("input[name=hidObjectTitle]").val() || "";
        let termId = $(e.currentTarget).parents("article").data("termid") || $("input[name=hidTermId]").val() || "";
        //
        let parentId = $(e.currentTarget).parents(".item-comment") ? $(e.currentTarget).parents(".item-comment").data("id") : 0;
        let sticker = $(e.currentTarget).parents(".form-info").siblings(".wrapper-txtarea").find(".chosen-ticker").attr("data-sticker");
        let contentInput = $(e.currentTarget).parents(".form-info").siblings(".form-group").find("textarea");
        let emailInput = $(e.currentTarget).parents(".form-info").find("input[name='inputEmail']");
        let nameInput = $(e.currentTarget).parents(".box-info").find("input[name='inputName']");
        let oUrl = $(e.currentTarget).parents("article").data("ourl") || window.location.pathname;
        // validate data to send
        if (!!(helper.checkContent(contentInput, sticker) && helper.checkEmail(emailInput))) {
            let content = helper.removeEmoji(contentInput.val());
            let email = emailInput.val();
            let name = nameInput.val() || emailInput.val().replace(/@.*/, "");
            return {
                object_id: objectId,
                app_id: this.appId,
                object_title: objectTitle,
                object_url: oUrl,
                term_id: termId,
                author_name: name,
                author_email: email,
                parent_id: parentId,
                content: content,
                sticker: sticker,
            };
        } else
            return false;
    }

    /* 10. send comment from event send-comment*/
    sendComment(data) {
        $.ajax({
            url: AppConfig.idDomainCM + '/api/send-comment',
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function (resolve) {
                helper.setCookieComment('cm-ttc',data.content,5);
                $('#modalSuccess').modal('show');
                setTimeout(function () {
                    $('#modalSuccess').modal('hide');
                }, 1500);
            },
            error: function () {
                $('#modalSuccess').modal('show');
                setTimeout(function () {
                    $('#modalSuccess').modal('hide');
                }, 1500);
            }
        });
    }

    /* 11. send report from event send-report*/
    sendReport(cmId, oId, re, oReason, reason) {
        $.ajax({
            url: AppConfig.idDomainCM + '/api/report-comment',
            type: 'POST',
            data: {
                'comment_id': cmId,
                'reporter_email': re,
                'report_content': oReason ? oReason : reason,
                'object_id': oId,
            },
            dataType: 'json',
            success: function (data) {
                $("#warningModal").modal('hide');
                $('#modalSuccess').modal('show');
                setTimeout(function () {
                    $('#modalSuccess').modal('hide');
                }, 1500);
            },
            error: function (err) {
                $("#warningModal").modal('hide');
                $('#modalSuccess').modal('show');
                setTimeout(function () {
                    $('#modalSuccess').modal('hide');
                }, 1500);
            }
        });
    }

}

$(() => {
    let keySortLike = $('input[name=hidKeyListCmSortLike]').val() || "";
    let keySortDate = $('input[name=hidKeyListCmNewest]').val() || "";
    new Comment(keySortLike, keySortDate);
});

