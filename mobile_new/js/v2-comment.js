// require import lib-comment.js before\

/*
@author Duynn
@time 16-10-2019
@email wasahara11111@gmail.com
*/

/*
class comment
* 1. constructor
* 2. init
* 3. generate block
* 4. event (load , write , like , get sticker , report , send ...)
* 5. map data to DOM
* 6. load more comment
* 7. write comment
* 8. create sticker
* 9. like comment
* 10. send comment
* 11. report comment
* */
class Comment {

    /* 1. cons*/
    constructor(keySortLike, keySortDate = "") {
        this.urlCommentLike = keySortLike;
        this.urlCommentDate = keySortDate;
        this.urlSticker = AppConfig.domainCdnHtml + "/" + AppConfig.keyThemeSticker + ".json";
        this.stickerList = [];
        this.stickerListTheme = [];
        this.commentDate = {};
        this.commentLike = {};
        this.appId = 15;
        this.cookieLikeComment = JSON.parse(helper.getCookie("like-ttc") ? helper.getCookie("like-ttc") : "[]");
        this.currentBtnSticker = null;
        this.init();
    }

    /* 2. contains initial comment*/
    init() {
        this.event();
        let ajaxSticker = $.ajax(
            {
                url: this.urlSticker,
                timeout: 5000
            }
        );
        ajaxSticker
            .fail(err => {
                let emoji = this.createSticker(this.stickerList);
                $(".comment-write").prepend($(this.writeComment()));
                this.initComment(this.urlCommentLike, "like")
            })
            .done(stickers => {
                this.stickerList = stickers ? JSON.parse(stickers).data : [];
                let emoji = this.createSticker(this.stickerList);
                $(".comment-write").prepend($(this.writeComment()));
                this.initComment(this.urlCommentLike, "like")
            });
    }

    /* 3. initial comment*/
    initComment(urlComment, type) {
        let ajaxComments = $.ajax(
            {
                url: AppConfig.domainCdnHtml + "/" + urlComment + "1.json",
            }
        );
        ajaxComments
            .done((listComments) => {
                if (Object.keys(JSON.parse(listComments)).length > 0) {
                    if (type === "like") {
                        this.commentLike = JSON.parse(listComments);
                        this.commentLike.pageNum = 1;
                        this.commentLike.currentNum = this.commentLike.number[0];
                        this.generateBlockComment(this.commentLike, type);
                    } else if (type === "date") {
                        this.commentDate = JSON.parse(listComments);
                        this.commentDate.pageNum = 1;
                        this.commentDate.currentNum = this.commentLike.number[0];
                        this.generateBlockComment(this.commentDate, type);
                    }
                }
            }).fail(err => {
            $(".wrapper-comment").empty();
        })
    }

    generateBlockComment(resolve, type) {
        let emoji = this.createSticker(this.stickerList);
        $("#stickerModal .modal-body").append(emoji);
        let comments = resolve.data;
        //prepend tool comment
        let wrap = $(".comment--main .wrapper-comment");
        let viewMore = $(`<a href="#" class="more-comment">Xem thêm 10 bình luận</a>`);
        $(".comment--main .detail-comment").prepend(viewMore);
        wrap.empty();
        if (comments.length > 0) {
            //map data to comment
            comments.forEach((comment, index) => {
                wrap.append(`${this.mapDataToComment(comment, index)}`);
                if (index===0){
                    $(".overview-comment").append(`${this.mapFirstComment(comment, index)}`);
                }
            });
        }
    }

    /* 4. contains all handle event*/
    event() {

        //handle load more comment
        this.eventLoadMoreComment();

        // handle write comment
        this.eventWriteComment();

        //show more comment
        this.eventShowMoreComment();

        // like comment
        this.eventLikeComment();

        //get sticker
        this.eventGetSticker();

        //handle report
        this.eventReportComment();

        //handle send comment
        this.eventSendComment();

        //handle switch list
        this.eventSwitchComment();

        //others
        this.eventOthers();
    }

    eventLoadMoreComment() {
        $(document).on("click", ".viewmore", e => {
            $(e.currentTarget).before(helper.createCommentHolder());
            let type = $(e.currentTarget).data('filter');
            $(e.currentTarget).hide();
            e.preventDefault();
            let emoji = this.createSticker(this.stickerList);
            this.loadMore(type, (data) => {
                let moreCmt = data.data.slice(data.currentNum - data.number[data.pageNum - 1]);
                $(".item-comment__holder").remove();
                if (moreCmt.length > 0) {
                    moreCmt.forEach((comment, index) => {
                        $(e.target).before(`${this.mapDataToComment(comment, index, emoji)}`);
                    });
                }
                if (data.currentNum >= data.total_parent)
                    $(e.target).remove();
                else {
                    $(e.target).show();
                    $(e.target).html(`Xem thĂªm  (${data.number[data.pageNum]}) bĂ¬nh luáº­n`);
                }
            });
        });
    }

    eventWriteComment() {
        //write comment
        $(document).on('click', '.btn-writecomment', e => {
            e.preventDefault();
            let anchor = $(e.currentTarget);
            $('html, body').animate({
                scrollTop: ($(anchor.attr('href')).offset().top - 200)
            }, 1000);
            $(anchor.attr('href')).focus();
        });
        // show box for enter info reply comment
        $(document).on("click", '.comment-tool .rep-comment', function (e) {
            let text = $(this).parent().siblings(".comment-box").find("textarea").val();
            let boxCm = $(this).parent().siblings(".comment-box");
            boxCm.slideToggle().find("textarea").val("").val(text).focus();
            $(".wrapper-comment").find(".comment-box")
                .not(boxCm)
                .slideUp();
        });
        // toggle box-info
        $(document).on('focus', ".detail-comment textarea", function () {
            $(this).parents().siblings(".form-info").slideDown();
        });
        // event focus input name
        $(document).on('focus', 'input[name=inputName]', function () {
            let email = $(this).parent().find('input[name=inputEmail]').val();
            let name = email.replace(/@.*/, "");
            $(this).val(name);
        });
        $(document).on('focus', '.comment-box textarea,.comment-box input,.modal input', function () {
            $(this).next(".error").fadeOut();
        });

    }

    eventLikeComment() {
        //like comment
        $(document).on("click", '.comment-tool .like-comment', e => {
            let objectId = $(e.target).parents('article').attr('data-oid')
                ? $(e.target).parents('article').attr('data-oid') : $("input[name=hidObjectId]").val(),
                commentId = $(e.target).parents(".comment-media").data("cmid");
            let likeField = $(e.currentTarget).find('.number-like');
            if (this.cookieLikeComment.find(comment => comment["comment-id"] === commentId) === undefined) {
                this.likeComment(objectId, commentId, likeField);
                $(e.currentTarget).text("Đã thích");
            }
        });
    }

    eventGetSticker() {
        //handle chose sticker
        $(document).on('click', '#stickerModal ul.list-sticker li',  (e) => {
            let current = $(e.currentTarget);
            let sources = current.attr('data-src'), icon = current.attr('data-stick');
            current.parents('#stickerModal').modal("hide");
            let choStick = this.currentBtnSticker.parents(".comment-box").find('.chosen-ticker');
            choStick = !choStick.length ? this.currentBtnSticker.parents(".comment-write").find('.chosen-ticker') : choStick;
            choStick.attr('data-src', sources);
            choStick.attr('data-sticker', icon);
            let img = document.createElement('img');
            img.src = sources;
            img.setAttribute('width', '100px');
            img.setAttribute('height', '100px');
            choStick.html(img);
        }); // event choose sticker for comment's sticker

        $(document).on('click', '.slick-sticker .i-sticker', e => {
            const themeId = $(e.currentTarget).data('themeid');
            this.findStickerInLocal(themeId, e);

        }); // get list sticker for sticker's theme

        /* handle event*/
        $(document).on("click", ".btn-sticker", e => {
            this.currentBtnSticker = $(e.currentTarget);
            let id = $("#stickerModal").find(".i-sticker[data-index=1]").data("themeid");
            this.findStickerInLocal(id, e);
        });

    }

    eventReportComment() {
        $(document).on('click', '.tool-like .report', function () {
            let cmid = $(this).parent().attr('data-cmid'),
                oid = $(this).parents('article').attr('data-oid')
                    ? $(this).parents('article').attr('data-oid')
                    : $("input[name=hidObjectId]").val();
            $('input[name=hidCCommentId]').val(cmid);
            $('input[name=hidCObjectId]').val(oid);
        }); // get info comment before report's popup is show

        $("select#reason").change(function () {
            let selected = $(this).children("option:selected").val();
            if (parseInt(selected) === 0) {
                $("#warningModal .box-other-reason").show();
            } else {
                $("#warningModal .box-other-reason").hide();
                $('textarea').val('');
            }
        }); // get reason for comment's report

        $(document).on('click', '#warningModal .btn-send-1', e => {
            let cmId = $('input[name=hidCCommentId]').val(),
                reInput = $('input[name=reporter_email]'),
                re = reInput.val(),
                oReason = $('#warningModal textarea').val(),
                reason = $('select#reason').find("option:selected").val(),
                oId = $('input[name=hidCObjectId]').val();
            if (helper.checkEmail(reInput))
                this.sendReport(cmId, oId, re, oReason, reason);
        }); // report comment
    }

    eventShowMoreComment() {
        $(document).on('click', '.show-child-comment', function () {
            $(this).nextAll(".comment").slideDown();
            $(this).remove();
        });
    }

    eventSendComment() {
        $(document).on("click", '.detail-comment .btn--comment', e => {
            const data = this.getAllToSend(e);
            if (data) {
                this.sendComment(data);
                $(e.currentTarget).parents().siblings(".form-group").find("textarea").val("");
                $(e.currentTarget).parents('.form-info').hide();
                $(e.currentTarget).parents(".form-info").siblings(".chosen-ticker").attr('data-sticker', null).html('');
            }

        }); // comment for article
    }

    eventSwitchComment() {
        $(document).on("click", ".tool-comment a", e => {
            $(e.currentTarget).addClass("active").siblings("a").removeClass("active");
            let type = $(e.currentTarget).data("filter");
            if (type === "like") {
                if (Object.keys(this.commentLike).length === 0)
                    this.initComment(this.urlCommentLike, type);
                else
                    this.generateBlockComment(this.commentLike, type)
            } else if (type === "date") {
                if (Object.keys(this.commentDate).length === 0)
                    this.initComment(this.urlCommentDate, type);
                else
                    this.generateBlockComment(this.commentDate, type)
            }
        })
    }

    eventOthers() {
        $(document).on("click", ".btn-group-sticker", function (e) {
            $(e.target).siblings(".error").hide();
        });
        $(document).on("click", "pre a .fa", function () {
            let boxContent = $(this).parents("pre");
            if ($(this).hasClass('fa-plus-square-o')) {
                boxContent.hide();
                boxContent.siblings().show();
                return '';
            }
            boxContent.hide();
            boxContent.siblings('pre').show();

        });
    }


    /* 5. map data to parent comment*/
    mapDataToComment(comment, index = 0) {
        let userImg = comment.author_name.trim().slice(0, 1).toUpperCase();
        let sticker = comment.sticker ? (comment.sticker.length !== 0 ? `<div class="comment-ticker"><img src="${comment.sticker.avatar_base_url + "/" + comment.sticker.avatar_path}" alt=""/></div>` : "") : "";
        let childComment = comment.child_comments ? (comment.child_comments !== '' ? this.mapDataToChildComment(comment.child_comments) : "") : "";
        return `<li class="comment" data-id="${comment.id}" style="animation-delay: ${index / 20}s">
                                <span class="comment-avatar" style="background-color: ${helper.randomColorInArray(helper.ARRAY_GLOBAL_COLOR)}">${userImg}</span>
                                <div class="comment-media" data-cmid="${comment.id}" data-author="${comment.author_name}">
                                    <div class="comment-content">
                                        <span class="comment-name">
                                        ${comment.author_name}
                                        </span>
                                        <span class="comment-text">
                                        ${comment.content}
                                        </span>
                                        ${sticker}
                                    </div>
                                    <div class="comment-tool">
                                        <span class="like-comment">${this.findCommentLiked(parseInt(comment.id)) ? "Đã thích" : "Thích"}</span>
                                        <span class="rep-comment">Trả lời</span>
                                        <span class="time">${helper.timeSince(comment.created_at * 1000)}</span>
                                    </div>
                                    <div class="comment-box">
                                        <div class="chosen-ticker"></div>
                                        <div class="form-group">
                                            <textarea type="text" aria-label=""
                                                      placeholder="Nhập bình luận ...">@${comment.author_name}&nbsp;</textarea>
                                            <i class="error error-email" style="display: none">Vui lòng nhập bình luận !</i>
                                            <div class="btn-group-sticker" data-toggle="modal" data-target="#stickerModal">
                                            <span class="btn-sticker"></span></div>
                                        </div>
                                        <div class="form-info text-right">
                                            <input aria-label="" type="text" name="inputEmail" placeholder="Email">
                                            <i class="error error-email" style="display: none">Hãy nhập Email !</i>
                                            <input aria-label="" type="text" name="inputName" placeholder="Họ và tên">
                                            <i class="error error-name" style="display: none">Hãy nhập họ và tên !</i>
                                            <button class="btn--comment">Gửi</button>
                                        </div>
                                    </div>
                                    ${comment.child_comments.length ? `<a class="show-child-comment" href="javascript: void(0)">
                                    ${comment.child_comments.length} phản hồi</a>`
            + childComment
            : ""}
                            </div>
        </li>
`;
    }

    /* 5. map data to child comment*/
    mapDataToChildComment(comments, emoji) {
        let listChild = "";
        comments.forEach(comment => {
            let userImg = comment.author_name.trim().slice(0, 1).toUpperCase();
            let sticker = comment.sticker ? (comment.sticker.length !== 0 ? `<div class="chosen-ticker"><img src="${comment.sticker.avatar_base_url + "/" + comment.sticker.avatar_path}" alt=""/></div>` : "") : "";
            listChild += `<div class="comment" style="display: none" data-childId="${comment.id}" data-parentId="${comment.parent_id}">
                                <span class="comment-avatar" style="background-color: ${helper.randomColorInArray(helper.ARRAY_GLOBAL_COLOR)}">${userImg}</span>
                                <div class="comment-media" data-cmid="${comment.id}" data-author="${comment.author_name}">
                                    <div class="comment-content">
                                        <span class="comment-name">
                                        ${comment.author_name}
                                        </span>
                                        <span class="comment-text">
                                        ${comment.content}
                                        </span>
                                        ${sticker}
                                    </div>
                                    <div class="comment-tool">
                                        <span class="like-comment">${this.findCommentLiked(parseInt(comment.id)) ? "Đã thích" : " Thích"}</span>
                                        <span class="rep-comment">Trả lời</span>
                                        <span class="time">${helper.timeSince(comment.created_at * 1000)}</span>
                                    </div>
                                    <div class="comment-box">
                                        <div class="chosen-ticker"></div>
                                        <div class="form-group">
                                            <textarea type="text" aria-label=""
                                                      placeholder="Nhập bình luận ...">@${comment.author_name}&nbsp;</textarea>
                                              <i class="error error-email" style="display: none">Vui lòng nhập bình luận !</i>
                                            <div class="btn-group-sticker" data-toggle="modal" data-target="#stickerModal">
                                            <span class="btn-sticker"></span></div>
                                        </div>
                                        <div class="form-info text-right">
                                            <input aria-label="" type="text" name="inputEmail" placeholder="Email">
                                            <i class="error error-email" style="display: none">Hãy nhập Email !</i>
                                            <input aria-label="" type="text" name="inputName" placeholder="Họ và tên">
                                            <i class="error error-name" style="display: none">Hãy nhập họ và tên !</i>
                                            <button class="btn--comment">Gửi</button>
                                        </div>
                                    </div>
                                  
                            </div>
        </div>`;
        });
        return listChild;
    }

    mapFirstComment(comment, index, emoji) {
        let userImg = comment.author_name.trim().slice(0, 1).toUpperCase();
        let sticker = comment.sticker ? (comment.sticker.length !== 0 ? `<div class="comment-ticker"><img src="${comment.sticker.avatar_base_url + "/" + comment.sticker.avatar_path}" alt=""/></div>` : "") : "";
        let childComment = comment.child_comments ? (comment.child_comments !== '' ? this.mapDataToChildComment(comment.child_comments, emoji) : "") : "";
        return `<div class="comment" data-id="${comment.id}" style="animation-delay: ${index / 20}s">
                                <span class="comment-avatar" style="background-color: ${helper.randomColorInArray(helper.ARRAY_GLOBAL_COLOR)}">${userImg}</span>
                                <div class="comment-media" data-cmid="${comment.id}" data-author="${comment.author_name}">
                                    <div class="comment-content">
                                        <span class="comment-name">
                                        ${comment.author_name}
                                        </span>
                                        <span class="comment-text">
                                        ${comment.content}
                                        </span>
                                        ${sticker}
                                    </div>
                                </div>
        </div>
`;
    }

    /* 6. show more comment from event load-more-comment*/
    loadMore(type, callback) {
        if (type === "like") {
            $.ajax({
                url: AppConfig.domainCdnHtml + "/" + this.urlCommentLike + `${this.commentLike.pageNum + 1}.json`,
            }).done(listComments => {
                let comments = JSON.parse(listComments);
                this.commentLike.currentNum += comments.number[this.commentLike.pageNum];
                this.commentLike.pageNum += 1;
                this.commentLike.data = this.commentLike.data.concat(comments.data);
                callback(this.commentLike);
            }).fail(err => {
                $(".block-comment a.viewmore").remove();
            })
            ;
        } else if (type === "date") {
            $.ajax({
                url: AppConfig.domainCdnHtml + "/" + this.urlCommentDate + `${this.commentDate.pageNum + 1}.json`,
            }).done(listComments => {
                let comments = JSON.parse(listComments);
                this.commentDate.currentNum += comments.number[this.commentDate.pageNum];
                this.commentDate.pageNum += 1;
                this.commentDate.data = this.commentDate.data.concat(comments.data);
                callback(this.commentDate);
            }).fail(err => {
                $(".block-comment a.viewmore").remove();
            })
            ;
        }

    }

    /* 7. write comment from event write-comment*/
    writeComment() {
        return `
                          <div class="chosen-ticker"></div>
                            <div class="form-group">
                                <textarea aria-label="" class="box-reply-cm" id="box-reply-cmt"
                                          placeholder="Comment đê! Ai cấm bạn cười..."></textarea>
                                <i class="error" style="display: none">Vui lòng nhập bình luận !</i>
                                <div class="btn-group-sticker" data-toggle="modal" data-target="#stickerModal"><span class="btn-sticker"></span>
                                </div>
                            </div>
                            <div class="form-info text-right">
                                <input aria-label="" type="text" name="inputEmail" placeholder="Email">
                                <i class="error error-email" style="display: none">Hãy nhập Email !</i>
                                <input aria-label="" type="text" name="inputName" placeholder="Họ và tên">
                                <i class="error error-name" style="display: none">Hãy nhập họ và tên !</i>
                                <button class="btn--comment">Gửi</button>
                            </div>
                       `;
    }

    /* 8. create sticker box*/
    createSticker() {
        let sticker = this.stickerList;
        let htmlSticker;
        if (sticker.length > 0) {
            htmlSticker = `<div class="custom-sticker">
                                                <div class="top-sticker">
                                                    <div class="inner-sticker">
                                                        <div class="btn-prev" title="previous" data-click="1" style="display: none">
                                                           
                                                        </div>
                                                        <div class="slick-sticker">
                                                            <div class="inner-slider-sticker">`;
            sticker.forEach((sticker, index) => {
                const stickersUrl = sticker.avatar_base_url + sticker.avatar_path;
                htmlSticker += `<div class="i-sticker" data-themeId="${sticker.id}" data-index=${index + 1}>
                                    <span class="btn-popover" data-toggle="tooltip" data-placement="top" title="${sticker.description}">
                                        <img alt="" src="${stickersUrl}">
                                    </span>
                                </div>`;
            });
            htmlSticker += `</div></div>
                                    <div class="btn-next" title="next" data-click="1">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="middle-sticker" >
                                    <ul class="list-sticker">
                                    </ul>
                            </div>
                        </div>`;
        } else {
            htmlSticker = "";
        }
        return htmlSticker;
    }

    /* 8. render sticker from event get sticker*/
    renderStickerByTheme(themeId, callback) {
        let url = AppConfig.domainCdnHtml + "/" + AppConfig.keySticker;

        $.ajax({
            url: url + "_" + themeId + ".json",
        }).done(data => {
            let stickers = JSON.parse(data).data;
            let html = '';
            stickers.forEach(function (item, index) {
                html += `<li data-stick="${item.id}" data-src="${item.avatar_base_url + "/" + item.avatar_path}">
                            <span><img class="action" src="${item.avatar_base_url + "/" + item.avatar_path}"></span>
                        </li>`;
            });
            callback(html);
        }).fail(err => {
            let html = "";
            callback(html);
        })
        ;

    }

    /* 8. find sticker in local*/
    findStickerInLocal(id, e) {
        if (!this.checkSticker(id) || this.stickerListTheme.length === 0) {
            this.renderStickerByTheme(id, html => {
                this.stickerListTheme.push({
                    html,
                    id
                });
                $("#stickerModal")
                    .find('.list-sticker')
                    .html(html);
            });
        } else {
            let resolve = this.stickerListTheme.find(item => {
                return parseInt(item.id) === parseInt(id);
            });
            $("#stickerModal")
                .find('.list-sticker')
                .html(resolve.html);
        }
    }

    /* 8. true or false sticker in local*/
    checkSticker(id) {
        return this.stickerListTheme.find(sticker => parseInt(sticker.id) === id);
    }

    /* 9. like comment from event like-comment*/
    likeComment(objectId, commentId, likeField) {
        let likeNumber = likeField.text();
        let comment = {
            "article-id": objectId,
            "comment-id": commentId
        };
        $.ajax({
            url: AppConfig.idDomainCM + '/api/vote-comment',
            type: 'POST',
            data: {
                'c': commentId,
                'app_id': this.appId,
                'object_id': objectId
            },
            dataType: 'json',
            success: data => {
                if (data.success === true) {
                    likeField.text(parseInt(likeNumber) + 1);
                    helper.setCookie("like-ttc", comment, 1440); // 1 ngĂ y
                    this.cookieLikeComment = JSON.parse(helper.getCookie("like-ttc") ? helper.getCookie("like-ttc") : "[]");
                }
            }
        });
    }

    /* 9. true or false liked comment in cookie*/
    findCommentLiked(id) {
        return (this.cookieLikeComment.find(comment => comment["comment-id"] === id));
    }

    /* 9. get all value need to send comment*/
    getAllToSend(e) {
        // get data to send
        let objectId = $(e.currentTarget).parents("article").data("oid") || $("input[name=hidObjectId]").val() || "";
        let objectTitle = $(e.currentTarget).parents("article").data("title") || $("input[name=hidObjectTitle]").val() || "";
        let termId = $(e.currentTarget).parents("article").data("termid") || $("input[name=hidTermId]").val() || "";
        //
        let parentId = $(e.currentTarget).parents(".comment") ? $(e.currentTarget).parents(".comment").data("id") : 0;
        let sticker = $(e.currentTarget).parents(".form-info").siblings(".chosen-ticker").attr("data-sticker");
        let contentInput = $(e.currentTarget).parents(".form-info").siblings(".form-group").find("textarea");
        let emailInput = $(e.currentTarget).parents(".form-info").find("input[name='inputEmail']");
        let nameInput = $(e.currentTarget).parents(".form-info").find("input[name='inputName']");
        let oUrl = $(e.currentTarget).parents("article").data("ourl") || window.location.pathname;
        // validate data to send
        if (!!(helper.checkContent(contentInput, sticker) && helper.checkEmail(emailInput))) {
            let content = helper.removeEmoji(contentInput.val());
            let email = emailInput.val();
            let name = nameInput.val() || emailInput.val().replace(/@.*/, "");
            return {
                object_id: objectId,
                app_id: this.appId,
                object_title: objectTitle,
                object_url: oUrl,
                term_id: termId,
                author_name: name,
                author_email: email,
                parent_id: parentId,
                content: content,
                sticker: sticker,
            };
        } else
            return false;
    }

    /* 10. send comment from event send-comment*/
    sendComment(data) {
        $.ajax({
            url: AppConfig.idDomainCM + '/api/send-comment',
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function (resolve) {
                helper.setCookieComment('cm-ttc', data.content, 5);
                $('#modalSuccess').modal('show');
                setTimeout(function () {
                    $('#modalSuccess').modal('hide');
                }, 1500);
            },
            error: function () {
                $('#modalSuccess').modal('show');
                setTimeout(function () {
                    $('#modalSuccess').modal('hide');
                }, 1500);
            }
        });
    }

    /* 11. send report from event send-report*/
    sendReport(cmId, oId, re, oReason, reason) {
        $.ajax({
            url: AppConfig.idDomainCM + '/api/report-comment',
            type: 'POST',
            data: {
                'comment_id': cmId,
                'reporter_email': re,
                'report_content': oReason ? oReason : reason,
                'object_id': oId,
            },
            dataType: 'json',
            success: function (data) {
                $("#warningModal").modal('hide');
                $('#modalSuccess').modal('show');
                setTimeout(function () {
                    $('#modalSuccess').modal('hide');
                }, 1500);
            },
            error: function (err) {
                $("#warningModal").modal('hide');
                $('#modalSuccess').modal('show');
                setTimeout(function () {
                    $('#modalSuccess').modal('hide');
                }, 1500);
            }
        });
    }
}

$(() => {
    let keySortLike = $('input[name=hidKeyListCmSortLike]').val() || "";
    let keySortDate = $('input[name=hidKeyListCmNewest]').val() || "";
    new Comment(keySortLike, keySortDate);
});

class CommentInTerm{
    constructor() {
        this.listComment = {};
        this.list = [];
        this.init();

    }

    init() {
        let articles = $(".list-in-term article.comment--term[data-comment]");
        let list = [];
        articles.each((_, article) => {
            list.push($(article).data('comment'));
        });
        this.list = list;
        if (this.list.length > 0)
            this.initComment();
    }

    /* 3. initial comment*/
    initComment() {
        this.list.forEach(urlComment => {
            if (urlComment){
                let ajaxComments = $.ajax(
                    {
                        url: AppConfig.domainCdnHtml + "/" + urlComment + "1.json",
                    }
                );
                ajaxComments
                    .done((listComments) => {
                        let list = JSON.parse(listComments);
                        this.listComment = {...this.listComment, [urlComment]: list};
                        this.generateBlockComment(urlComment);
                    }).fail(err => {
                    $(`article[data-comment="${urlComment}"]`).removeAttr("data-comment");
                })
            }
        })

    }

    generateBlockComment(id) {
        let article = $(`article[data-comment="${id}"]`);
        article.removeAttr("data-comment");
        let resolve = this.listComment[id];
        let comments = resolve.data;
        let wrap = article.find($(".wrapper-comment"));
        if (comments && comments.length > 0) {
            //map data to comment
            comments.slice(0,2).forEach((comment, index) => {
                wrap.append(`${this.mapDataToComment(comment, index)}`)
            });
        }
    }

    /* 5. map data to parent comment*/
    mapDataToComment(comment, index = 0) {
        let userImg = comment.author_name.trim().slice(0, 1).toUpperCase();
        let sticker = comment.sticker ? (comment.sticker.length !== 0 ? `<div class="comment-ticker"><img src="${comment.sticker.avatar_base_url + "/" + comment.sticker.avatar_path}" alt=""/></div>` : "") : "";
        return `<li class="comment" data-id="${comment.id}" style="animation-delay: ${index / 20}s">
                                <span class="comment-avatar" style="background-color: ${helper.randomColorInArray(helper.ARRAY_GLOBAL_COLOR)}">${userImg}</span>
                                <div class="comment-media" data-cmid="${comment.id}" data-author="${comment.author_name}">
                                    <div class="comment-content">
                                        <span class="comment-name">
                                        ${comment.author_name}
                                        </span>
                                        <span class="comment-text">
                                        ${comment.content}
                                        </span>
                                        ${sticker}
                                    </div>
                            </li>`;
    }
}

$(() => {
    let cmInTerm = new CommentInTerm();
});